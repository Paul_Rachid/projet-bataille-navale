/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bataillenavale;

/**
 * Case de base avec coordonnées x, y
 * @author Olivier
 */
public class Case {
//Attributs
    //Positions en x et y
    protected int x;
    protected int y;
    
    
    
    
/*______________*/
//Constructeurs
    /**
     * Constructeur par défaut sans paramètre
     */
    public Case(){
        this.x = 0;
        this.y = 0;
    }
    /**
     * Constructeur avec coordonnées
     * @param x
     * @param y 
     */
    public Case(int x, int y){
        this.x = x;
        this.y = y;
    }
    
    
    
    
/*______________*/
//Méthodes
    /**
     * Retourne coordonnée en x
     * @return 
     */
    public int getX(){
        return this.x;
    }
    
    /**
     * Retourne coordonnée en y
     * @return 
     */
    public int getY(){
        return this.y;
    }
    
    //Setters
    /**
     * Set coordonnée x
     * @param x 
     */
    public void setX(int x){
        this.x = x;
    }
    
    /**
     * Set coordonnée y
     * @param y 
     */
    public void setY(int y){
        this.y = y;
    }
    
    
    
    /**
     * Afficher les coordonnées de la cases
     */
    public void affiche(){
        System.out.println("x: " + this.x + "y: " + this.y);
    }
    
    /**
     * Incrémente x
     */
    public void incrementerX(){
        this.x = this.x + 1;
    }
    
    /**
     * Incrémente y
     */
    public void incrementerY(){
        this.y = this.y + 1;
    }
    
    /**
     * Décrémente x
     */
    public void decrementerX(){
        this.x = x - 1;
    }
    
    /**
     * Décrémente y
     */
    public void decrementerY(){
        this.y = y - 1;
    }
    
    
    
    /**
     * Déplace les cases d'une certaine valeur
     * @param dx
     * @param dy 
     */
    public void deplacer(int dx, int dy){
        this.x = this.x + dx;
        this.y = this.y + dy;
    }
}
