/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bataillenavale;

/**
 * Navire particulier - Croiseur
 * @author Olivier
 */
public class Croiseur extends Navire{
//Attributs
    private final int taille = 5;
    private final int puissance = 4;

    //Cases du croiseur
    private CaseNavire[] coordCroiseur;
    
    
    
/*______________*/
//Constructeurs
    /**
     * Constructeur par défaut
     */
    public Croiseur(){
        super();
        this.coordCroiseur = new CaseNavire[this.taille];
    }
    
    /**
     * Constructeur avec coordonnées de toutes les cases du croiseur en paramètres
     * @param coordCroiseur 
     */
    public Croiseur(CaseNavire[] coordCroiseur){
        this();
        int i;
        for(i=0;i<this.taille;i++)
        {
            this.coordCroiseur[i] = coordCroiseur[i];
        }
    }
    
    /**
     * Constructeur - non utilisé
     * @param nom
     * @param direction
     * @param coordCroiseur 
     */
    public Croiseur(String nom, boolean direction, CaseNavire[] coordCroiseur){
        super(nom, direction);
        this.coordCroiseur = coordCroiseur;
    }
    
    /**
     * Constructeur de croiseur avec tous les attributs nécessaires en paramètres lors de l'initialisation des croiseurs
     * @param nom
     * @param direction
     * @param coordCroiseur
     * @param symbole 
     */
    public Croiseur(String nom, boolean direction, CaseNavire[] coordCroiseur, String symbole){
        super(nom, direction, symbole);
        this.coordCroiseur = coordCroiseur;
    }
    
/*______________*/
//Méthodes

    @Override
    int getSize(){
        return this.taille;
    }
    
    @Override
    int getX(int n){
        return this.coordCroiseur[n].getX();
    }
    
    @Override
    int getY(int n){
        return this.coordCroiseur[n].getY();
    }

    
    @Override
    boolean getImpact(int x, int y){
        int n; //nombre de cases du croiseur
        boolean impact = false;
        for (n=0;n<this.taille;n++)
        {
            if ((this.coordCroiseur[n].getX() == x) && (this.coordCroiseur[n].getY() == y))
                impact = this.coordCroiseur[n].getImpact();
            
        }
        return impact;
    }
    
    
    @Override
    Case getCase(int n){
        return this.coordCroiseur[n];
    }
    
    @Override
    int getPuissance(){
        return this.puissance;
    }
    
    @Override
    CaseNavire getCaseNavire(int n){
        return this.coordCroiseur[n];
    }
}
