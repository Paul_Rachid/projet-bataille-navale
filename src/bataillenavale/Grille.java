/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bataillenavale;
import java.util.*;
/**
 * Grille (Vue)
 * @author Olivier
 */
public class Grille {
//Attributs    
    private CaseGrille[][] grille;
    private int taille;
    
    
    
/*_____________*/
//Constructeurs
    /**
     * Constructeur par défaut sans paramètre, taille 15 par défaut
     */
    public Grille(){
        this.grille = new CaseGrille[15][15];
        //Remplissage de la grille
        int i, j;
        for(i=0;i<15;i++)
        {
            for(j=0;j<15;j++)
            {
                this.grille[i][j] = new CaseGrille(i, j);
            }
        }
    }
    
    /**
     * Constructeur avec taille en paramètres, pour des grilles à taille variable
     * @param taille 
     */
    public Grille(int taille){
        this.taille = taille;
        //Initialisation de la grille
        this.grille = new CaseGrille[taille][taille];
        //Remplissage de la grille
        int i, j;
        for(i=0;i<taille;i++)
        {
            for(j=0;j<taille;j++)
            {
                grille[i][j].setX(i);
                grille[i][j].setY(j);
                grille[i][j].setOccup(false); //Case non occupée à l'initialisation
            }
        }
    }
    
/*_____________*/
//Méthodes
    //Getters
    /**
     * Get taille de la grille
     * @return 
     */
    public int getTaille(){
        return this.taille;
    }
    
    /**
     * Vérifie l'occupation de la case de coordonnées x et y
     * @param x
     * @param y
     * @return 
     */
    public boolean getOccupCaseGrille(int x, int y){
        return grille[x][y].getOccup();
    }
    
    
    //Setters
    /**
     * Modifie la taille de la grille
     * @param taille 
     */
    public void setTaille(int taille){
        this.taille = taille;
    }
    
    /**
     * Change la valeur du booléen en true de la case de coordonnées x,y
     * @param x
     * @param y 
     */
    public void setOccupTrue(int x, int y){
        this.grille[x][y].setOccup(true);
    }
    
    /**
     *Affiche une grille alliée, avec les symboles correspondant aux différents navires
     * @param Navires
     */
    public void afficherGrilleAlliee(ArrayList <Navire> Navires){
        //Nombre de navires à afficher
        int n;
        //Compteur pour afficher la grille
        int i, j;
        
        System.out.println("Votre grille avec vos navires : \n");
        
        for(i=0;i<15;i++)
        {
            for (j=0;j<15;j++)
            {
                //Affichage coordonnées
                if ((i==0)&&(j==0))
                    System.out.println("X  0  1  2  3  4  5  6  7  8  9 10 11 12 13 14 ");
                if (j==0)
                {
                    if (i==0)
                        System.out.print("A ");
                    if (i==1)
                        System.out.print("B ");
                    if (i==2)
                        System.out.print("C ");
                    if (i==3)
                        System.out.print("D ");
                    if (i==4)
                        System.out.print("E ");
                    if (i==5)
                        System.out.print("F ");
                    if (i==6)
                        System.out.print("G ");
                    if (i==7)
                        System.out.print("H ");
                    if (i==8)
                        System.out.print("I ");
                    if (i==9)
                        System.out.print("J ");
                    if (i==10)
                        System.out.print("K ");
                    if (i==11)
                        System.out.print("L ");
                    if (i==12)
                        System.out.print("M ");
                    if (i==13)
                        System.out.print("N ");
                    if (i==14)
                        System.out.print("O ");
                    
                }
                
                if ((this.grille[i][j].getOccup() == false))
                    System.out.print(" ~ ");
                
                
                if (this.grille[i][j].getOccup() == true)
                {
                    for (n=0;n<Navires.size();n++)
                    {
                        //Compteur taille des navires
                        int n1;
                        
                        for(n1=0;n1<Navires.get(n).getSize();n1++)
                        {
                            if ((this.grille[i][j].getX() == Navires.get(n).getX(n1))&&(this.grille[i][j].getY() == Navires.get(n).getY(n1)))
                                Navires.get(n).afficheSymbole();
                        }
                    }
                }
                
                
            }
            System.out.println("");
        }
        System.out.println("\n\n\n");
    }
    
    
    /**
     * Afficher la grille de son adversaire avec le symbole "X" sur les cases touchées, pour ne pas dévoiler la nature du navire touché
     * @param Navires 
     */
    public void afficherGrilleEnnemie(ArrayList <Navire> Navires){
        //Nombre de navires à considérer
        int n;
        //Compteur pour afficher la grille
        int i, j;
        
        System.out.println("La grille de votre adversaire : \n ");
        for(i=0;i<15;i++)
        {
            for (j=0;j<15;j++)
            {
                //Affichage coordonnées
                if ((i==0)&&(j==0))
                    System.out.println("X  0  1  2  3  4  5  6  7  8  9 10 11 12 13 14 ");
                if (j==0)
                {
                    if (i==0)
                        System.out.print("A ");
                    if (i==1)
                        System.out.print("B ");
                    if (i==2)
                        System.out.print("C ");
                    if (i==3)
                        System.out.print("D ");
                    if (i==4)
                        System.out.print("E ");
                    if (i==5)
                        System.out.print("F ");
                    if (i==6)
                        System.out.print("G ");
                    if (i==7)
                        System.out.print("H ");
                    if (i==8)
                        System.out.print("I ");
                    if (i==9)
                        System.out.print("J ");
                    if (i==10)
                        System.out.print("K ");
                    if (i==11)
                        System.out.print("L ");
                    if (i==12)
                        System.out.print("M ");
                    if (i==13)
                        System.out.print("N ");
                    if (i==14)
                        System.out.print("O ");
                    
                }
                
                if (this.grille[i][j].getOccup() == false)
                    System.out.print(" ~ ");
                if (this.grille[i][j].getOccup() == true)
                {
                    for (n=0;n<Navires.size();n++)
                    {
                        //Compteur taille des navires
                        int n1;
                        
                        for(n1=0;n1<Navires.get(n).getSize();n1++)
                        {
                            if ((this.grille[i][j].getX() == Navires.get(n).getX(n1))&&(this.grille[i][j].getY() == Navires.get(n).getY(n1))&&(Navires.get(n).getImpact(Navires.get(n).getX(n1), Navires.get(n).getY(n1))==true))
                                System.out.print(" X ");
                            if ((this.grille[i][j].getX() == Navires.get(n).getX(n1))&&(this.grille[i][j].getY() == Navires.get(n).getY(n1))&&(Navires.get(n).getImpact(Navires.get(n).getX(n1), Navires.get(n).getY(n1))==false))
                                System.out.print(" ~ ");
                        }
                    }
                }
                    
            }
            System.out.println("");
        }
        System.out.println("\n\n\n");
        
    }
}
