/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bataillenavale;
import java.util.*;
/**
 * Déroulement partie
 * @author Olivier
 */
public class Partie {
//Attributs
    private Joueur joueur1;
    private Joueur joueur2;
    private int tour;
    private String nom;
    
    
    
/*________________*/
//Constructeurs
    /**
     * Constructeur partie par défaut avec tour initialisé à 0
     */
    public Partie(){
        this.tour = 0;
    }
    
    /**
     * Constructeur avec possibilité de mettre un nom de partie (pour sauvegarde éventuelle)
     * @param nom 
     */
    public Partie(String nom){
        this.nom = nom;
    }
    
    

    
/*________________*/
//Méthodes
    //Getters
    /**
     * Retourne le numéro du tour
     * @return 
     */
    public int getTour(){
        return this.tour;
    }
    
    /**
     * Retourne le nom de la partie
     * @return 
     */
    public String getNom(){
        return this.nom;
    }
    
    //Setters
    /**
     * Change le nom de la partie
     * @param nom
     */
    public void setNom(String nom){
        this.nom = nom;
    }
    
    //Méthodes
    /**
     * Incrémente le tour 
     */
    public void incrementeTour(){
        this.tour = this.tour + 1;
    }
    
    
    
    public void lancerPartie(){
        int choix = 0; //Choix du joueur
        String rep;
        Scanner scan = new Scanner(System.in);
        joueur1 = new Joueur(1);
        joueur2 = new Joueur(2);
        
        joueur1.saisieNom();
        joueur2.saisieNom();
        
        joueur1.nouveauxNavires();
        joueur2.nouveauxNavires();
        
        joueur1.setGrilleEnnemie(joueur2.grilleAlliee);
        joueur2.setGrilleEnnemie(joueur1.grilleAlliee);
        
        do{
            
            
            System.out.println("\n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \nTour du joueur : " + joueur1.getNom());
            joueur1.grilleAlliee.afficherGrilleAlliee(joueur1.Navires);
            joueur1.grilleEnnemie.afficherGrilleEnnemie(joueur2.Navires);
       
            System.out.println("1: Tirer avec un navire   2: Déplacer un navire");
        
            do{
                choix = joueur1.choix();
            }while((choix!=1)&&(choix!=2));
            
            if(choix == 1)
            {
                joueur1.grilleEnnemie.afficherGrilleEnnemie(joueur2.Navires);
                joueur1.tirer(joueur1.Navires, joueur2.Navires);
                joueur1.grilleEnnemie.afficherGrilleEnnemie(joueur2.Navires);
                joueur2.checkViesNavires(joueur2.Navires);
                joueur1.refreshOccupGrilleAlliee(joueur2.Navires);
            }
            if(choix == 2)
            {
                
                joueur1.grilleAlliee.afficherGrilleAlliee(joueur1.Navires);
                joueur1.Navires = joueur1.deplacement(joueur1.Navires);
                joueur1.refreshOccupGrilleAlliee(joueur1.Navires);
                joueur1.grilleAlliee.afficherGrilleAlliee(joueur1.Navires);
                joueur2.refreshOccupGrilleEnnemie(joueur1.Navires);
            }
            
            do{
                System.out.println("Prêt à jouer " + joueur2.getNom() + " ?");
                System.out.println("o: oui");
                rep = scan.nextLine();
            }while(rep.contains("o")==false);
            
            
            System.out.println("\n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \nTour du joueur : " + joueur2.getNom());
            joueur2.grilleAlliee.afficherGrilleAlliee(joueur2.Navires);
            joueur2.grilleEnnemie.afficherGrilleEnnemie(joueur1.Navires);
            
            System.out.println("1: Tirer avec un navire   2: Déplacer un navire");
        
            do{
                choix = joueur2.choix();
            }while((choix!=1)&&(choix!=2));
            if(choix == 1)
            {
                joueur2.grilleEnnemie.afficherGrilleEnnemie(joueur1.Navires);
                joueur2.tirer(joueur2.Navires, joueur1.Navires);
                joueur2.grilleEnnemie.afficherGrilleEnnemie(joueur1.Navires);
                joueur1.checkViesNavires(joueur1.Navires);
                joueur1.refreshOccupGrilleAlliee(joueur1.Navires);
                
            }
            if(choix == 2)
            {
                
                joueur2.grilleAlliee.afficherGrilleAlliee(joueur2.Navires);
                joueur2.Navires = joueur2.deplacement(joueur2.Navires);
                joueur2.refreshOccupGrilleAlliee(joueur2.Navires);
                joueur2.grilleAlliee.afficherGrilleAlliee(joueur2.Navires);
                joueur1.refreshOccupGrilleEnnemie(joueur2.Navires);
            }
            
            do{
                System.out.println("Prêt à jouer " + joueur1.getNom() + " ?");
                System.out.println("o: oui");
                rep = scan.nextLine();
            }while(rep.contains("o")==false);
            
            this.tour = this.tour + 1;
            
        }while((!joueur1.Navires.isEmpty())&&(!joueur2.Navires.isEmpty()));
        
        if(joueur1.Navires.isEmpty())
            System.out.println("Félicitations " + joueur2.getNom() + " !!! Vous avez gagné !!!");
        else
            System.out.println("Félicitations " + joueur1.getNom() + " !!! Vous avez gagné !!!");
        
    }
}
