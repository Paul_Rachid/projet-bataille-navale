/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bataillenavale;

/**
 * Case Navire hérite de case pour constituer les navires
 * @author Olivier
 */
public class CaseNavire extends Case{
//Attributs
    //Impact sur la case du bateau
    private boolean impact; //true = impact sur case, false = pas d'impact

    
    
/*______________*/
//Constructeurs
    /**
     * Constructeur par défaut 
     * Impact initialisé à false de base (car navire non touché)
     */
    public CaseNavire(){
        super();
        this.impact = false;
    }
    
    /**
     * Constructeur avec coordonnées en paramètres pour placer les cases des navires
     * @param x
     * @param y 
     */
    public CaseNavire(int x, int y){
        super(x, y);
        this.impact = false;
    }
    
    
/*______________*/
//Méthodes
    //Getters
    /**
     * Getter booléen impact
     * @return 
     */
    public boolean getImpact(){
        return this.impact;
    }
    
    //Setters
    /**
     * Modifie la valeur de impact avec la valeur en paramètre
     * @param impact 
     */
    public void setImpact(boolean impact){
        this.impact = impact;
    }
    
    
}
