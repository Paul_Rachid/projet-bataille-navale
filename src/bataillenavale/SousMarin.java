/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bataillenavale;

/**
 * Navire particulier - Sous-Marin
 * @author Olivier
 */
public class SousMarin extends Navire{
//Attributs
    private final int taille = 1;
    private final int puissance = 1;

    //Case du Sous-Marin
    private CaseNavire coordSousMarin;
    
    
    
/*______________*/
//Constructeur
    /**
     * Constructeur par défaut, appelle le constructeur de la classe mère
     */
    public SousMarin(){
        super();
    }
    
    /**
     * Constructeur - non utilisé
     * @param coordSousMarin 
     */
    public SousMarin(CaseNavire coordSousMarin){
        this();
        this.coordSousMarin = coordSousMarin;
    }
    
    /**
     * Constructeur - non utilisé
     * @param nom
     * @param direction
     * @param coordSousMarin
    */
    public SousMarin(String nom, boolean direction, CaseNavire coordSousMarin){
        super(nom, direction);
        this.coordSousMarin = coordSousMarin;
    }
    
    /**
     * Constructeur avec tous les attributs nécessaires en paramètres lors de l'initialisation des sous-marins
     * @param nom
     * @param direction
     * @param coordSousMarin
     * @param symbole 
     */
    public SousMarin(String nom, boolean direction, CaseNavire coordSousMarin, String symbole){
        super(nom, direction, symbole);
        this.coordSousMarin = coordSousMarin;
    }
    
    
/*______________*/
//Méthodes
    //Getters
    /**
     * Renvoie les coordonnées du sous-marin
     * @return 
     */
    public CaseNavire getCoordSousMarin(){
        return this.coordSousMarin;
    }
    
    
    
    //Setters
    /**
     * Change les coordonnées du sous-marin
     * @param coord 
     */
    public void setCoordSousMarin(CaseNavire coord){
        this.coordSousMarin = coord;
    }

    
    @Override
    int getSize(){
        return this.taille;
    }
    
    @Override
    int getX(int n){
        return this.coordSousMarin.getX();
    }
    
    @Override
    int getY(int n){
        return this.coordSousMarin.getY();
    }
    

    
    @Override
    boolean getImpact(int x, int y){
        return this.coordSousMarin.getImpact();
    }
    
    @Override
    Case getCase(int n){
        return this.coordSousMarin;
    }
    
    @Override
    int getPuissance(){
        return this.puissance;
    }
    
    @Override
    CaseNavire getCaseNavire(int n){
        return coordSousMarin;
    }
}
