/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bataillenavale;

/**
 * Navire particulier - Destroyer
 * @author Olivier
 */
public class Destroyer extends Navire{
//Attributs
    private final int taille = 3;
    private final int puissance = 1;
    private final String symbole = "3";
    //Cases du destroyer
    private CaseNavire[] coordDestroyer;
    
    
    
/*______________*/
//Constructeurs
    /**
     * Constructeur par défaut sans paramètre
     */
    public Destroyer(){
        super();
        this.coordDestroyer = new CaseNavire[this.taille];
    }
    
    /**
     * Constructeur avec coordonnées du Destroyer en paramètres
     * @param coordDestroyer 
     */
    public Destroyer(CaseNavire[] coordDestroyer){
        this();
        int i;
        for(i=0;i<this.taille;i++)
        {
            this.coordDestroyer[i] = coordDestroyer[i];
        }
    }
    
    /**
     * Constructeur - non utilisé
     * @param nom
     * @param direction
     * @param coordDestroyer 
     */
    public Destroyer(String nom, boolean direction, CaseNavire[] coordDestroyer)
    {
        super(nom, direction);
        this.coordDestroyer = coordDestroyer;
        
    }
    
    /**
     * Constructeur avec tous les attributs en paramètres nécessaires à l'initialisation des Destroyers
     * @param nom
     * @param direction
     * @param coordDestroyer
     * @param symbole 
     */
    public Destroyer(String nom, boolean direction, CaseNavire[] coordDestroyer, String symbole)
    {
        super(nom, direction, symbole);
        this.coordDestroyer = coordDestroyer;
        
    }
    
    
/*______________*/
//Méthodes

    @Override
    int getSize(){
        return this.taille;
    }
    
    @Override
    int getX(int n){
        return this.coordDestroyer[n].getX();
    }
    
    @Override
    int getY(int n){
        return this.coordDestroyer[n].getY();
    }

    
    @Override
    boolean getImpact(int x, int y){
        int n; //nombre de cases du croiseur
        boolean impact = false;
        for (n=0;n<this.taille;n++)
        {
            if ((this.coordDestroyer[n].getX() == x) && (this.coordDestroyer[n].getY() == y))
                impact = this.coordDestroyer[n].getImpact();
            
        }
        return impact;
    }
    
    @Override
    Case getCase(int n){
        return this.coordDestroyer[n];
    }
    
    @Override
    int getPuissance(){
        return this.puissance;
    }
    
    @Override
    CaseNavire getCaseNavire(int n){
        return this.coordDestroyer[n];
    }
}
