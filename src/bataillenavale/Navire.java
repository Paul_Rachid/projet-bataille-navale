/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bataillenavale;

/**
 *Classe abstraite Navire
 * @author Olivier
 */
public abstract class Navire{
//Attributs
    protected String nom;
    protected boolean dispotir; //le navire est-il disponible pour tirer ?
    protected boolean dispodep; //le navire est-il disponible pour se déplacer ?
    protected boolean direction; //True = horizontal, false = vertical
    protected String symbole;

    
/*_____________*/
//Constructeurs
    /**
     * Constructeur par défaut sans paramètre
     */
    public Navire(){
        this.dispotir = true;
        this.dispodep = true;
    }
    
    /**
     * Constructeur avec booléen direction en paramètres
     * @param direction 
     */
    public Navire(boolean direction){
        this();
        this.direction = direction;
    }

    /**
     * Constructeur avec nom en paramètres
     * @param nom 
     */
    public Navire(String nom){
        this();
        this.nom = nom;
    }
    
    /**
     * Constructeur avec nom et booléen direction en paramètres
     * @param nom
     * @param direction 
     */
    public Navire(String nom, boolean direction){
        this();
        this.nom = nom;
        this.direction = direction;
    }
    
    /**
     * Constructeur avec tous les attributs nécessaires pour la future initialisation de tous les types de navires
     * @param nom
     * @param direction
     * @param symbole 
     */
    public Navire(String nom, boolean direction, String symbole){
        this();
        this.nom = nom;
        this.direction = direction;
        this.symbole = symbole;
        
    }
    
/*_____________*/
//Méthodes
    //Getters
    /**
     * Retourne le nom du navire
     * @return 
     */
    public String getNom(){
        return this.nom;
    }
    
    /**
     * Retourne le booléen dispotir pour savoir si le navire peut tirer
     * @return 
     */
    public boolean getDispoTir(){
        return this.dispotir;
    }
    
    /**
     * Retourne le booléen dispodep pour savoir si le navire peut se déplacer
     * @return 
     */
    public boolean getDispoDep(){
        return this.dispodep;
    }
    
    /**
     * Retourne le booléen direction pour savoir dans quels sens le navire peut se déplacer
     * @return 
     */
    public boolean getDirection(){
        return this.direction;
    }
    
    
    //Setters
    /**
     * Modifie le nom
     * @param nom 
     */
    public void setNom(String nom){
        this.nom = nom;
    }
    
    /**
     * Modifie le symbole sur grille
     * @param symbole 
     */
    public void setSymbole(String symbole){
        this.symbole = symbole;
    }
    
    /**
     * Change la disponibilité du navire pour les tirs
     * @param dispotir 
     */
    public void setDispoTir(boolean dispotir){
        this.dispotir = dispotir;
    }
    
    /**
     * Change la disponibilité du navire pour les déplacements
     * @param dispodep 
     */
    public void setDispoDep(boolean dispodep){
        this.dispodep = dispodep;
    }
    
    /**
     * Change la direction du navire pour savoir dans quels sens les déplacements sont possibles
     * @param direction 
     */
    public void setDirection(boolean direction){
        this.direction = direction;
    }
    
    /**
     * Affiche le symbole du navire sur la grille
     */
    public void afficheSymbole(){
        System.out.print(" " + this.symbole + " ");
    }
    
    //Méthodes abstraites
    /**
     * Méthode abstraite pour avoir la taille du navire
     * @return 
     */
    abstract int getSize();
    
    /**
     * Méthode abstraite pour avoir la coordonnée en x de la case numéro n
     * @param n
     * @return 
     */
    abstract int getX(int n);
    
    /**
     * Méthode abstraite pour avoir la coordonnée en y de la case numéro n
     * @param n
     * @return 
     */
    abstract int getY(int n);
    
    /**
     * Méthode abstraite pour avoir la puissance de tir du navire
     * @return 
     */
    abstract int getPuissance();
    
    /**
     * Méthode abstraite pour savoir si la case en paramètres a été touchée
     * @param x
     * @param y
     * @return 
     */
    abstract boolean getImpact(int x, int y);
    
    /**
     * Méthode abstraite pour avoir la case numéro n du navire
     * @param n
     * @return 
     */
    abstract Case getCase(int n);
    
    /**
     * Méthode abstraite pour avoir la case numéro n du navire + le booléen impact
     * @param n
     * @return 
     */
    abstract CaseNavire getCaseNavire(int n);
    
    //Méthodes

    /**
     *Affiche le nom du navire
     */
    public void afficherNavire(){
        System.out.println(this.nom);
    }
    
    
}
