/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bataillenavale;
import java.util.*;

/**
 * Joueur avec son nom, son équipe, ses grilles, ses navires 
 * @author Olivier
 */
public class Joueur {
//Attributs
    private String nom;
    private int equipe;
    protected Grille grilleAlliee;
    protected Grille grilleEnnemie;
    protected ArrayList Navires;
    
    
    
    
/*________________*/
//Constructeurs
    /**
     * Constructeur joueur avec numéro d'équipe en paramètres
     * @param equipe 
     */
    public Joueur(int equipe){
        this.equipe = equipe;
        this.grilleAlliee = new Grille();
        this.grilleEnnemie = new Grille();
        this.Navires = new ArrayList();
    }
    
    /**
     * Constructeur joueur avec tous les attributs en paramètres (pour un éventuel chargement par exemple)
     * @param nom
     * @param equipe
     * @param grilleAlliee
     * @param grilleEnnemie
     * @param Navires 
     */
    public Joueur(String nom, int equipe, Grille grilleAlliee, Grille grilleEnnemie, ArrayList Navires){
        this.nom = nom;
        this.equipe = equipe;
        this.grilleAlliee = grilleAlliee;
        this.grilleEnnemie = grilleEnnemie;
        this.Navires = Navires;
    }
    

    
/*________________*/
//Méthodes
    //Getters
    /**
     * Retourne le nom du joueur
     * @return 
     */
    public String getNom(){
        return this.nom;
    }
    
    /**
     * Retourne l'équipe du joueur
     * @return 
     */
    public int getEquipe(){
        return this.equipe;
    }
    
    /**
     * Retourne la grille des navires du joueur en train de jouer
     * @return 
     */
    public Grille getGrilleAlliee(){
        return this.grilleAlliee;
    }
    
    /**
     * Retourne la grille des navires du joueur ennemi
     * @return 
     */
    public Grille getGrilleEnnemie(){
        return this.grilleEnnemie;
    }
    
    /**
     * Retourne la liste des navires du jooueur
     * @return 
     */
    public ArrayList getNavires(){
        return this.Navires;
    }
    
    //Setters
    /**
     * Modifie le nom du joueur
     * @param nom 
     */
    public void setNom(String nom){
        this.nom = nom;
    }
    
    /**
     * Modifie l'équipe du joueur
     * @param equipe 
     */
    public void setEquipe(int equipe){
        this.equipe = equipe;
    }
    
    /**
     * Remplace la grille alliée avec la grille en paramètres
     * @param grilleAlliee 
     */
    public void setGrilleAlliee(Grille grilleAlliee){
        this.grilleAlliee = grilleAlliee;
    }
    
    /**
     * Remplace la grille ennemie par la grille en paramètres
     * @param grilleEnnemie 
     */
    public void setGrilleEnnemie(Grille grilleEnnemie){
        this.grilleEnnemie = grilleEnnemie;
    }
    
    /**
     * Remplace les navires du joueur par les navires en paramètres
     * @param Navires 
     */
    public void setNavires(ArrayList Navires){
        this.Navires = Navires;
    }
    
    
    
    //Saisie des noms
    /**
     * Saisie du nom du joueur
     */
    public void saisieNom(){
        System.out.println("Saisissez le nom du joueur" + equipe + ":");
        Scanner sc = new Scanner(System.in);
        this.nom = sc.nextLine();
    }
    
    
    /**
     * Initialisation de tous les navires du joueur
     */
    public void nouveauxNavires(){
        //Coordonnées d'une nouvelle CaseNavire
        int x;
        int y;
        
        //Nombre aléatoire pour générer coordonnées
        Random rand = new Random();
        int random;
        String nouveaunom = "Navire";
        String symbole;
        
        symbole = "?";
        
        //Création sous-marin
        int nbS = 0;
        
        
        do
        {
            if (nbS == 0)
                nouveaunom = "Sous-Marin 1";
            if (nbS == 1)
                nouveaunom = "Sous-Marin 2";
            if(nbS == 2)
                nouveaunom = "Sous-Marin 3";
            if(nbS == 3)
                nouveaunom = "Sous-Marin 4";
            
            
            x = rand.nextInt(15);
            y = rand.nextInt(15);
            
            if((this.grilleAlliee.getOccupCaseGrille(x,y)) == false)
            {
                CaseNavire casesousmarin = new CaseNavire(x,y);
                random = rand.nextInt(2);
                if (nbS == 0)
                    symbole = "0";
                if (nbS == 1)
                    symbole = "1";
                if(nbS == 2)
                    symbole = "2";
                if(nbS == 3)
                    symbole = "3";
                
                if (random == 0)
                {
                    SousMarin sousmarin = new SousMarin (nouveaunom, true, casesousmarin, symbole);
                    this.grilleAlliee.setOccupTrue(x, y);
                    this.Navires.add(sousmarin);
                    nbS = nbS + 1;
                }
                if (random == 1)
                {
                    SousMarin sousmarin = new SousMarin (nouveaunom, false, casesousmarin, symbole);
                    this.grilleAlliee.setOccupTrue(x, y);
                    this.Navires.add(sousmarin);
                    nbS = nbS + 1;
                }
                if ((random != 0) && (random != 1))
                    System.out.println("Bug init sous-marin" + random);
            }
            
        }while(nbS != 4);
        
        //Création destroyers
        //Nombre de destroyers placés
        int nbD = 0;
        
        
        do{
            x = rand.nextInt(15);
            y = rand.nextInt(15);
            
            if (nbD == 0)
                nouveaunom = "Destroyer 1";
            if (nbD ==1)
                nouveaunom = "Destroyer 2";
            if(nbD == 2)
                nouveaunom = "Destroyer 3";
            
            
            if((this.grilleAlliee.getOccupCaseGrille(x,y)) == false)
            {
                //On détermine si le destroyer est horizontal ou vertical et on vérifie les futures cases du destroyer
                random = rand.nextInt(2);
                if (nbD == 0)
                    symbole = "4";
                if (nbD ==1)
                    symbole = "5";
                if(nbD == 2)
                    symbole = "6";
                
                if (random == 0)
                {
                    if((x>1)&&(x<13)&&(this.grilleAlliee.getOccupCaseGrille(x,y) == false)&&(this.grilleAlliee.getOccupCaseGrille(x-1,y) == false)&&(this.grilleAlliee.getOccupCaseGrille(x+1,y) == false))
                    {
                        CaseNavire case1destroyer = new CaseNavire(x-1,y);
                        CaseNavire case2destroyer = new CaseNavire(x,y);
                        CaseNavire case3destroyer = new CaseNavire(x+1,y);
                        
                        CaseNavire coordDestroyer[] = {case1destroyer, case2destroyer, case3destroyer};
                        
                        this.grilleAlliee.setOccupTrue(x-1, y);
                        this.grilleAlliee.setOccupTrue(x, y);
                        this.grilleAlliee.setOccupTrue(x+1, y);
                        
                        Destroyer destroyer = new Destroyer(nouveaunom, true, coordDestroyer, symbole);
                        this.Navires.add(destroyer);
                        nbD = nbD + 1;
                    }
                }
                if (random == 1)
                {
                    if((y>1)&&(y<13)&&(this.grilleAlliee.getOccupCaseGrille(x,y) == false)&&(this.grilleAlliee.getOccupCaseGrille(x,y-1) == false)&&(this.grilleAlliee.getOccupCaseGrille(x,y+1) == false))
                    {
                        CaseNavire case1destroyer = new CaseNavire(x,y-1);
                        CaseNavire case2destroyer = new CaseNavire(x,y);
                        CaseNavire case3destroyer = new CaseNavire(x,y+1);
                        
                        CaseNavire coordDestroyer[] = {case1destroyer, case2destroyer, case3destroyer};
                        
                        this.grilleAlliee.setOccupTrue(x, y-1);
                        this.grilleAlliee.setOccupTrue(x, y);
                        this.grilleAlliee.setOccupTrue(x, y+1);
                        
                        Destroyer destroyer = new Destroyer(nouveaunom, false, coordDestroyer, symbole);
                        this.Navires.add(destroyer);
                        nbD = nbD + 1;
                    }
                }
                if ((random != 0) && (random != 1))
                    System.out.println("Bug init Destroyer" + random);
            }
            
        }while (nbD != 3);
        
        
        //Création croiseurs
        int nbCr = 0;
        do{
            if (nbCr == 0)
                nouveaunom = "Croiseur 1";
            if (nbCr == 1)
                nouveaunom = "Croiseur 2";
            
            x = rand.nextInt(15);
            y = rand.nextInt(15);
            
            
            if((this.grilleAlliee.getOccupCaseGrille(x,y)) == false)
            {
                //On détermine si le croiseur est horizontal ou vertical et on vérifie les futures cases du destroyer
                random = rand.nextInt(2);
                if (nbCr == 0)
                    symbole = "7";
                if (nbCr == 1)
                    symbole = "8";
                if (random == 0)
                {
                    if((x>2)&&(x<12)&&(this.grilleAlliee.getOccupCaseGrille(x,y) == false)&&(this.grilleAlliee.getOccupCaseGrille(x-1,y) == false)&&(this.grilleAlliee.getOccupCaseGrille(x+1,y) == false)&&(this.grilleAlliee.getOccupCaseGrille(x-2, y) == false)&&(this.grilleAlliee.getOccupCaseGrille(x+2, y) == false))
                    {
                        CaseNavire case1croiseur = new CaseNavire(x-2,y);
                        CaseNavire case2croiseur = new CaseNavire(x-1,y);
                        CaseNavire case3croiseur = new CaseNavire(x,y);
                        CaseNavire case4croiseur = new CaseNavire(x+1,y);
                        CaseNavire case5croiseur = new CaseNavire(x+2,y);
                        
                        CaseNavire coordCroiseur[] = {case1croiseur, case2croiseur, case3croiseur, case4croiseur, case5croiseur};
                        
                        this.grilleAlliee.setOccupTrue(x-2, y);
                        this.grilleAlliee.setOccupTrue(x-1, y);
                        this.grilleAlliee.setOccupTrue(x, y);
                        this.grilleAlliee.setOccupTrue(x+1, y);
                        this.grilleAlliee.setOccupTrue(x+2, y);
                        
                        Croiseur croiseur = new Croiseur(nouveaunom, true, coordCroiseur, symbole);
                        this.Navires.add(croiseur);
                        nbCr = nbCr + 1;
                    }
                }
                if (random == 1)
                {
                    if((y>2)&&(y<12)&&(this.grilleAlliee.getOccupCaseGrille(x,y) == false)&&(this.grilleAlliee.getOccupCaseGrille(x,y-1) == false)&&(this.grilleAlliee.getOccupCaseGrille(x,y+1) == false)&&(this.grilleAlliee.getOccupCaseGrille(x,y+2) == false)&&(this.grilleAlliee.getOccupCaseGrille(x,y-2) == false))
                    {
                        CaseNavire case1croiseur = new CaseNavire(x,y-2);
                        CaseNavire case2croiseur = new CaseNavire(x,y-1);
                        CaseNavire case3croiseur = new CaseNavire(x,y);
                        CaseNavire case4croiseur = new CaseNavire(x,y+1);
                        CaseNavire case5croiseur = new CaseNavire(x,y+2);
                        
                        CaseNavire coordCroiseur[] = {case1croiseur, case2croiseur, case3croiseur, case4croiseur, case5croiseur};
                        
                        this.grilleAlliee.setOccupTrue(x, y-2);
                        this.grilleAlliee.setOccupTrue(x, y-1);
                        this.grilleAlliee.setOccupTrue(x, y);
                        this.grilleAlliee.setOccupTrue(x, y+1);
                        this.grilleAlliee.setOccupTrue(x, y+2);
                        
                        Croiseur croiseur = new Croiseur(nouveaunom, false, coordCroiseur, symbole);
                        this.Navires.add(croiseur);
                        nbCr = nbCr + 1;
                    }
                }
                if ((random != 0) && (random != 1))
                    System.out.println("Bug init croiseur" + random);
            }
            
        }while(nbCr != 2);
        
        
        //Création cuirrassé
        int nbCui = 0;
        nouveaunom = "Cuirassé";
        symbole = "9";
        do{
            
            x = rand.nextInt(15);
            y = rand.nextInt(15);
            
            if((this.grilleAlliee.getOccupCaseGrille(x,y)) == false)
            {
                //On détermine si le croiseur est horizontal ou vertical et on vérifie les futures cases du destroyer
                random = rand.nextInt(2);
                if (random == 0)
                {
                    if((x>3)&&(x<11)&&(this.grilleAlliee.getOccupCaseGrille(x,y) == false)&&(this.grilleAlliee.getOccupCaseGrille(x-1,y) == false)&&(this.grilleAlliee.getOccupCaseGrille(x+1,y) == false)&&(this.grilleAlliee.getOccupCaseGrille(x-2, y) == false)&&(this.grilleAlliee.getOccupCaseGrille(x+2, y) == false)&&(this.grilleAlliee.getOccupCaseGrille(x+3,y) == false)&&(this.grilleAlliee.getOccupCaseGrille(x-3,y) == false))
                    {
                        CaseNavire case1cuirasse = new CaseNavire(x-3,y);
                        CaseNavire case2cuirasse = new CaseNavire(x-2,y);
                        CaseNavire case3cuirasse = new CaseNavire(x-1,y);
                        CaseNavire case4cuirasse = new CaseNavire(x,y);
                        CaseNavire case5cuirasse = new CaseNavire(x+1,y);
                        CaseNavire case6cuirasse = new CaseNavire(x+2,y);
                        CaseNavire case7cuirasse = new CaseNavire(x+3,y);
                        
                        CaseNavire coordCuirasse[] = {case1cuirasse, case2cuirasse, case3cuirasse, case4cuirasse, case5cuirasse,case6cuirasse, case7cuirasse};
                        
                        this.grilleAlliee.setOccupTrue(x-3, y);
                        this.grilleAlliee.setOccupTrue(x-2, y);
                        this.grilleAlliee.setOccupTrue(x-1, y);
                        this.grilleAlliee.setOccupTrue(x, y);
                        this.grilleAlliee.setOccupTrue(x+1, y);
                        this.grilleAlliee.setOccupTrue(x+2, y);
                        this.grilleAlliee.setOccupTrue(x+3, y);
                        
                        Cuirasse cuirasse = new Cuirasse(nouveaunom, true, coordCuirasse, symbole);
                        this.Navires.add(cuirasse);
                        nbCui = nbCui + 1;
                    }
                }
                if (random == 1)
                {
                    if((y>3)&&(y<10)&&(this.grilleAlliee.getOccupCaseGrille(x,y) == false)&&(this.grilleAlliee.getOccupCaseGrille(x,y-1) == false)&&(this.grilleAlliee.getOccupCaseGrille(x,y+1) == false)&&(this.grilleAlliee.getOccupCaseGrille(x,y+2) == false)&&(this.grilleAlliee.getOccupCaseGrille(x,y-2) == false)&&(this.grilleAlliee.getOccupCaseGrille(x,y-3) == false)&&(this.grilleAlliee.getOccupCaseGrille(x,y+3) == false))
                    {
                        CaseNavire case1cuirasse = new CaseNavire(x,y-3);
                        CaseNavire case2cuirasse = new CaseNavire(x,y-2);
                        CaseNavire case3cuirasse = new CaseNavire(x,y-1);
                        CaseNavire case4cuirasse = new CaseNavire(x,y);
                        CaseNavire case5cuirasse = new CaseNavire(x,y+1);
                        CaseNavire case6cuirasse = new CaseNavire(x,y+2);
                        CaseNavire case7cuirasse = new CaseNavire(x,y+3);
                        
                        CaseNavire coordCuirasse[] = {case1cuirasse, case2cuirasse, case3cuirasse, case4cuirasse, case5cuirasse,case6cuirasse, case7cuirasse};
                        
                        this.grilleAlliee.setOccupTrue(x, y-3);
                        this.grilleAlliee.setOccupTrue(x, y-2);
                        this.grilleAlliee.setOccupTrue(x, y-1);
                        this.grilleAlliee.setOccupTrue(x, y);
                        this.grilleAlliee.setOccupTrue(x, y+1);
                        this.grilleAlliee.setOccupTrue(x, y+2);
                        this.grilleAlliee.setOccupTrue(x, y+3);
                        
                        Cuirasse cuirasse = new Cuirasse(nouveaunom, false, coordCuirasse, symbole);
                        this.Navires.add(cuirasse);
                        nbCui = nbCui + 1;
                    }
                }
                if ((random != 0) && (random != 1))
                    System.out.println("Bug init cuirassé" + random);
            }
            
        }while(nbCui != 1);
        
        
    
    }
    
    
    public int choix(){
        Scanner sc = new Scanner (System.in);
        int choix = sc.nextInt();
        return choix;
    }
    
    
    
    /**
     * Déplacement des navires 
     * @param Navires
     * @return 
     */
    public ArrayList <Navire> deplacement(ArrayList <Navire> Navires){
        int i; //Choix du navire à déplacer
        int d; //Choix de la direction du déplacement
        int n; //Compteur nombre de navires
        int c; //Nombre de cases du navire
        int x, y; //Coordonnées
        
        
        boolean test; //vérification de condition, true = opération possible
        
        
        System.out.println("Navires disponibles pour un déplacement : ");
        for(n=0;n<Navires.size();n++)
        {
            if(Navires.get(n).getDispoDep()== true)
            {
                System.out.print(n + " : ");
                Navires.get(n).afficherNavire();
            }
        }
        
        //Scanner pour prendre la saisie du joueur
        Scanner sc = new Scanner(System.in);
        
        do{
            test = true;
            do{
                System.out.println("Sélectionnez le navire à déplacer : ");
                i = sc.nextInt();
            }while(Navires.get(i).getDispoDep() == false);
            
            
            if (Navires.get(i).getDirection() == true)
            {
                System.out.println("1: Déplacement vers le haut   2: Déplacement vers le bas");
                d = sc.nextInt();
            
                if (d==1)
                {
                    for(c=0;c<Navires.get(i).getSize();c++)
                    {
                        if((Navires.get(i).getX(c)<1))
                        {
                            test = false;
                            System.out.println("Déplacement impossible, veuillez réessayer.");
                        }

                    }
                    
                    x = Navires.get(i).getX(0);
                    y = Navires.get(i).getY(0);
                        
                    if((test == true) && (this.grilleAlliee.getOccupCaseGrille(x-1, y) == true))
                    {
                        test = false;
                        System.out.println("Case bloquée !");
                    }
                    
                    
                        
                    
                    if (test == true)
                    {
                        for(c=0;c<Navires.get(i).getSize();c++)
                        {
                            Navires.get(i).getCase(c).decrementerX();
                        }
                        System.out.println("Déplacement en cours...");
                    }
                }
                
                if (d==2)
                {
                    for(c=0;c<Navires.get(i).getSize();c++)
                    {
                        if (Navires.get(i).getX(c)>13)
                        {
                            test = false;
                            System.out.println("Déplacement impossible, veuillez réessayer");
                        }
                        
                    }
                    
                    x = Navires.get(i).getX(Navires.get(i).getSize()-1);
                    y = Navires.get(i).getY(Navires.get(i).getSize()-1);
                        
                    if((test == true) && (this.grilleAlliee.getOccupCaseGrille(x+1, y) == true))
                    {
                        test = false;
                        System.out.println("Case bloquée !");
                    }
                    
                   
                    
                    if (test == true)
                    {
                        for (c=0;c<Navires.get(i).getSize();c++)
                        {
                            Navires.get(i).getCase(c).incrementerX();
                        }
                        System.out.println("Déplacement en cours...");
                        
                    }
                }
                if ((d!=1)&&(d!=2))
                {
                    System.out.println("Choix impossible");
                    test = false;
                }
                    
            }
            
            if (Navires.get(i).getDirection() == false)
            {
                System.out.println("1: Déplacement vers la gauche   2: Déplacement vers la droite");
                d = sc.nextInt();
            
                if (d==1)
                {
                    for(c=0;c<Navires.get(i).getSize();c++)
                    {
                        if(Navires.get(i).getY(c)<1)
                        {
                            test = false;
                            System.out.println("Déplacement impossible, veuillez réessayer.");
                        }
                        
                    }
                    
                    x = Navires.get(i).getX(0);
                    y = Navires.get(i).getY(0);
                        
                    if((test == true) && (this.grilleAlliee.getOccupCaseGrille(x, y-1) == true))
                    {
                        test = false;
                        System.out.println("Case bloquée !");
                    }
                    
                    if (test == true)
                    {
                        for(c=0;c<Navires.get(i).getSize();c++)
                        {
                            Navires.get(i).getCase(c).decrementerY();
                        }
                        System.out.println("Déplacement en cours...");
                    }
                }
                
                if (d==2)
                {
                    for(c=0;c<Navires.get(i).getSize();c++)
                    {
                        if (Navires.get(i).getY(c)>13)
                        {
                            test = false;
                            System.out.println("Déplacement impossible, veuillez réessayer");
                        }
                        
                    }
                    
                    x = Navires.get(i).getX(Navires.get(i).getSize()-1);
                    y = Navires.get(i).getY(Navires.get(i).getSize()-1);
                        
                    if((test == true) && (this.grilleAlliee.getOccupCaseGrille(x, y+1) == true))
                    {
                        test = false;
                        System.out.println("Case bloquée !");
                    }
                    
                    
                    if (test == true)
                    {
                        for (c=0;c<Navires.get(i).getSize();c++)
                        {
                            Navires.get(i).getCase(c).incrementerY();
                        }
                        System.out.println("Déplacement en cours...");
                    }
                }
                if ((d!=1)&&(d!=2))
                {
                    System.out.println("Choix impossible");
                    test = false;
                }
                    
            }
            
        }while(test == false);
        
        System.out.println("Déplacement réussi.");
        return this.Navires;
        
    }
    
    
    
    /**
     * Refresh les occupations des grilles 
     * @param Navires 
     */
    public void refreshOccupGrilleAlliee(ArrayList <Navire> Navires){
        int n; //nombre de navires
        int c; //nombre de cases des navires
        this.grilleAlliee = new Grille();
        //Coordonnées des cases
        int x;
        int y;
        
        for (n=0;n<Navires.size();n++)
        {
            for (c=0;c<Navires.get(n).getSize();c++)
            {
                x = Navires.get(n).getX(c);
                y = Navires.get(n).getY(c);
                this.grilleAlliee.setOccupTrue(x, y);
            }
            
        }
    }
    
    /**
     * Refresh les occupations des grilles 
     * @param Navires 
     */
    public void refreshOccupGrilleEnnemie(ArrayList <Navire> Navires){
        int n; //nombre de navires
        int c; //nombre de cases des navires
        this.grilleEnnemie = new Grille();
        //Coordonnées des cases
        int x;
        int y;
        
        for (n=0;n<Navires.size();n++)
        {
            for (c=0;c<Navires.get(n).getSize();c++)
            {
                x = Navires.get(n).getX(c);
                y = Navires.get(n).getY(c);
                this.grilleEnnemie.setOccupTrue(x, y);
            }
            
        }
    }
    
    /**
     * Gère les tirs 
     * @param NaviresAllies
     * @param NaviresEnnemis 
     */
    public void tirer(ArrayList <Navire> NaviresAllies, ArrayList <Navire> NaviresEnnemis){
        int n; //Compteur - nombre de navires
        int c; //Compteur - nombre de cases des navires
        int i; //Choix du navire
        int tir; //Type de tir - Puissance
        String ligne;
        
        int x = 100; //Coordonnées de tir
        int y = 100;
        boolean test; 
        boolean touche = false;
        
        Scanner sc = new Scanner (System.in);
        Scanner scan = new Scanner (System.in);
        
        System.out.println("Navires disponibles pour tirer :");
        
        for (n=0;n<NaviresAllies.size();n++)
        {
            if (NaviresAllies.get(n).getDispoTir()== true)
            {
                System.out.print(n + " : ");
                NaviresAllies.get(n).afficherNavire();
            }
                
        }
        
        do{
            System.out.println("Sélectionnez le navire pour tirer :");
            i = sc.nextInt();
        }while((i<0)&&(i>9)&&(NaviresAllies.get(i).getDispoTir()==false));
        
        tir = NaviresAllies.get(i).getPuissance();
        
        do{
            System.out.println("Sélectionnez les coordonnées (lettre minuscule de la ligne + numéro de colonne) :");
            ligne = scan.nextLine();
            test = (ligne.contains("a")|| ligne.contains("b")|| ligne.contains("c")|| ligne.contains("d")|| ligne.contains("e")|| ligne.contains("f")|| ligne.contains("g")|| ligne.contains("h")|| ligne.contains("i")|| ligne.contains("j")|| ligne.contains("k")|| ligne.contains("l")|| ligne.contains("m")|| ligne.contains("n")|| ligne.contains("o"))&&(ligne.contains("1")||ligne.contains("2")||ligne.contains("3")||ligne.contains("4")||ligne.contains("5")||ligne.contains("6")||ligne.contains("7")||ligne.contains("8")||ligne.contains("9")||ligne.contains("10")||ligne.contains("11")||ligne.contains("12")||ligne.contains("13")||ligne.contains("14"));
        }while(test == false);
        
        if (ligne.contains("a"))
            x = 0;
        if (ligne.contains("b"))
            x = 1;
        if (ligne.contains("c"))
            x = 2;
        if (ligne.contains("d"))
            x = 3;
        if (ligne.contains("e"))
            x = 4;
        if (ligne.contains("f"))
            x = 5;
        if (ligne.contains("g"))
            x = 6;
        if (ligne.contains("h"))
            x = 7;
        if (ligne.contains("i"))
            x = 8;
        if (ligne.contains("j"))
            x = 9;
        if (ligne.contains("k"))
            x = 10;
        if (ligne.contains("l"))
            x = 11;
        if (ligne.contains("m"))
            x = 12;
        if (ligne.contains("n"))
            x = 13;
        if (ligne.contains("o"))
            x = 14;
        
        if (ligne.contains("1") && (ligne.contains("10")==false) && (ligne.contains("11")) && (ligne.contains("12")==false) && (ligne.contains("13")==false) && (ligne.contains("14")==false))
            y = 1;
        if (ligne.contains("2") &&  (ligne.contains("12")==false))
            y = 2;
        if (ligne.contains("3") &&  (ligne.contains("13")==false))
            y = 3;
        if (ligne.contains("4") &&  (ligne.contains("14")==false))
            y = 4;
        if (ligne.contains("5"))
            y = 5;
        if (ligne.contains("6"))
            y = 6;
        if (ligne.contains("7"))
            y = 7;
        if (ligne.contains("8"))
            y = 8;
        if (ligne.contains("9"))
            y = 9;
        if (ligne.contains("10"))
            y = 10;
        if (ligne.contains("11"))
            y = 11;
        if (ligne.contains("12"))
            y = 12;
        if (ligne.contains("13"))
            y = 13;
        if (ligne.contains("14"))
            y = 14;
        
        if (tir == 1)
        {
            for (n=0;n<NaviresEnnemis.size();n++)
            {
                for(c=0;c<NaviresEnnemis.get(n).getSize();c++)
                {
                    if ((NaviresEnnemis.get(n).getX(c) == x)&&(NaviresEnnemis.get(n).getY(c) == y))
                    {
                        touche = true;
                        NaviresEnnemis.get(n).getCaseNavire(c).setImpact(true);
                        NaviresEnnemis.get(n).setDispoDep(false);
                    }
                        
                }
            }
        }
        
        if (tir == 4)
        {
            for (n=0;n<NaviresEnnemis.size();n++)
            {
                for(c=0;c<NaviresEnnemis.get(n).getSize();c++)
                {
                    if ((NaviresEnnemis.get(n).getX(c) == x)&&(NaviresEnnemis.get(n).getY(c) == y))
                    {
                        touche = true;
                        NaviresEnnemis.get(n).getCaseNavire(c).setImpact(true);
                        NaviresEnnemis.get(n).setDispoDep(false);
                    }
                    if ((NaviresEnnemis.get(n).getX(c) == x+1)&&(NaviresEnnemis.get(n).getY(c) == y))
                    {
                        touche = true;
                        NaviresEnnemis.get(n).getCaseNavire(c).setImpact(true);
                        NaviresEnnemis.get(n).setDispoDep(false);
                    }
                    if ((NaviresEnnemis.get(n).getX(c) == x-1)&&(NaviresEnnemis.get(n).getY(c) == y))
                    {
                        touche = true;
                        NaviresEnnemis.get(n).getCaseNavire(c).setImpact(true);
                        NaviresEnnemis.get(n).setDispoDep(false);
                    }
                    if ((NaviresEnnemis.get(n).getX(c) == x)&&(NaviresEnnemis.get(n).getY(c) == y-1))
                    {
                        touche = true;
                        NaviresEnnemis.get(n).getCaseNavire(c).setImpact(true);
                        NaviresEnnemis.get(n).setDispoDep(false);
                    }
                    if ((NaviresEnnemis.get(n).getX(c) == x)&&(NaviresEnnemis.get(n).getY(c) == y+1))
                    {
                        touche = true;
                        NaviresEnnemis.get(n).getCaseNavire(c).setImpact(true);
                        NaviresEnnemis.get(n).setDispoDep(false);
                    }
                        
                }
            }
        }
        
        
        if (tir == 9)
        {
            for (n=0;n<NaviresEnnemis.size();n++)
            {
                for(c=0;c<NaviresEnnemis.get(n).getSize();c++)
                {
                    if ((NaviresEnnemis.get(n).getX(c) == x)&&(NaviresEnnemis.get(n).getY(c) == y))
                    {
                        touche = true;
                        NaviresEnnemis.get(n).getCaseNavire(c).setImpact(true);
                        NaviresEnnemis.get(n).setDispoDep(false);
                    }
                    if ((NaviresEnnemis.get(n).getX(c) == x+1)&&(NaviresEnnemis.get(n).getY(c) == y))
                    {
                        touche = true;
                        NaviresEnnemis.get(n).getCaseNavire(c).setImpact(true);
                        NaviresEnnemis.get(n).setDispoDep(false);
                    }
                    if ((NaviresEnnemis.get(n).getX(c) == x-1)&&(NaviresEnnemis.get(n).getY(c) == y))
                    {
                        touche = true;
                        NaviresEnnemis.get(n).getCaseNavire(c).setImpact(true);
                        NaviresEnnemis.get(n).setDispoDep(false);
                    }
                    if ((NaviresEnnemis.get(n).getX(c) == x)&&(NaviresEnnemis.get(n).getY(c) == y-1))
                    {
                        touche = true;
                        NaviresEnnemis.get(n).getCaseNavire(c).setImpact(true);
                        NaviresEnnemis.get(n).setDispoDep(false);
                    }
                    if ((NaviresEnnemis.get(n).getX(c) == x)&&(NaviresEnnemis.get(n).getY(c) == y+1))
                    {
                        touche = true;
                        NaviresEnnemis.get(n).getCaseNavire(c).setImpact(true);
                        NaviresEnnemis.get(n).setDispoDep(false);
                    }
                    if ((NaviresEnnemis.get(n).getX(c) == x-1)&&(NaviresEnnemis.get(n).getY(c) == y-1))
                    {
                        touche = true;
                        NaviresEnnemis.get(n).getCaseNavire(c).setImpact(true);
                        NaviresEnnemis.get(n).setDispoDep(false);
                    }
                    if ((NaviresEnnemis.get(n).getX(c) == x+1)&&(NaviresEnnemis.get(n).getY(c) == y+1))
                    {
                        touche = true;
                        NaviresEnnemis.get(n).getCaseNavire(c).setImpact(true);
                        NaviresEnnemis.get(n).setDispoDep(false);
                    }
                    if ((NaviresEnnemis.get(n).getX(c) == x-1)&&(NaviresEnnemis.get(n).getY(c) == y+1))
                    {
                        touche = true;
                        NaviresEnnemis.get(n).getCaseNavire(c).setImpact(true);
                        NaviresEnnemis.get(n).setDispoDep(false);
                    }
                    if ((NaviresEnnemis.get(n).getX(c) == x+1)&&(NaviresEnnemis.get(n).getY(c) == y-1))
                    {
                        touche = true;
                        NaviresEnnemis.get(n).getCaseNavire(c).setImpact(true);
                        NaviresEnnemis.get(n).setDispoDep(false);
                    }
                        
                }
            }
        }
        
        if (touche == true)
            System.out.println("\n\nBim! Touché !");
        else
            System.out.println("\n\nRaté ! Dommage...");
        
        
    }
    
    
    /**
     * Vérifie les vies des navires, les retire du jeu si ces derniers sont morts
     * @param Navires 
     */
    public void checkViesNavires(ArrayList <Navire> Navires){
        int n; //nombre de navires
        int c; //cases des navires
        int vies; //nombres de "vies" du navire
        
        
        for(n=0;n<Navires.size();n++)
        {
            vies = Navires.get(n).getSize();
            for(c=0;c<Navires.get(n).getSize();c++)
            {
                if (Navires.get(n).getCaseNavire(c).getImpact()==true)
                {
                    vies = vies - 1;
                }
            }
            if (vies == 0)
            {
                System.out.println("\n\n" + Navires.get(n).getNom() + " a été coulé avec succès ! Bien joué mon capitaine !");
                Navires.remove(n);
            }
            
        }
        
    }
    
    
    
}
