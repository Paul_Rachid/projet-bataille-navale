/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bataillenavale;

/**
 * Navire particulier - Cuirassé
 * @author Olivier
 */
public class Cuirasse extends Navire{
//Attributs
    private final int taille = 7;
    private final int puissance = 9;
    private final String symbole = "7";
    //Cases du cuirrassé
    private CaseNavire[] coordCuirasse;
    
    
    
    
/*_____________*/
//Constructeurs
    /**
     * Constructeur par défaut
     */
    public Cuirasse(){
        super();
        this.coordCuirasse = new CaseNavire[this.taille];
    }
    
    /**
     * Constructeur avec coordonnées du Cuirassé en paramètres
     * @param coordCuirasse 
     */
    public Cuirasse(CaseNavire[] coordCuirasse){
        this();
        int i;
        for(i=0;i<this.taille;i++)
        {
            this.coordCuirasse[i] = coordCuirasse[i];
        }    
    }
    
    /**
     * Constructeur non utilisé
     * @param nom
     * @param direction
     * @param coordCuirasse 
     */
    public Cuirasse(String nom, boolean direction, CaseNavire[] coordCuirasse){
        super(nom, direction);
        this.coordCuirasse= coordCuirasse;
    }
    
    /**
     * Constructeur avec tous les attributs nécessaires à l'initialisation des cuirassés en paramètres
     * @param nom
     * @param direction
     * @param coordCuirasse
     * @param symbole 
     */
    public Cuirasse(String nom, boolean direction, CaseNavire[] coordCuirasse, String symbole){
        super(nom, direction, symbole);
        this.coordCuirasse= coordCuirasse;
    }
   
    
    
/*______________*/
//Méthodes

    @Override
    int getSize(){
        return this.taille;
    }
    
    @Override
    int getX(int n){
        return this.coordCuirasse[n].getX();
    }
    
    @Override
    int getY(int n){
        return this.coordCuirasse[n].getY();
    }

    
    @Override
    boolean getImpact(int x, int y){
        int n; //nombre de cases du croiseur
        boolean impact = false;
        for (n=0;n<this.taille;n++)
        {
            if ((this.coordCuirasse[n].getX() == x) && (this.coordCuirasse[n].getY() == y))
                impact = this.coordCuirasse[n].getImpact();
            
        }
        return impact;
    }
    
    @Override
    Case getCase(int n){
        return this.coordCuirasse[n];
    }
    
    @Override
    int getPuissance(){
        return this.puissance;
    }
    
    @Override
    CaseNavire getCaseNavire(int n){
        return this.coordCuirasse[n];
    }
}
