/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bataillenavale;
import java.util.*;
/**
 *  pour vérifier le bon fonctionnement de certains outils
 * @author Olivier
 */
public class Test {
    int i;
    
    public Test(){
        Random rnd = new Random();
        i = rnd.nextInt(1);
    }
    
    public int getI(){
        return this.i;
    }
    
    public static void main(String[] args){
        Test test = new Test();
        
        System.out.println(test.getI());
    }
    
}
