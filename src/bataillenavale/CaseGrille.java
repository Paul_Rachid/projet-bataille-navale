/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bataillenavale;

/**
 * Case pour grille hérite de case (+ booléen occup pour savoir si la case est occupée)
 * @author Olivier
 */
public class CaseGrille extends Case {
//Attributs
    //Case occupée ou non
    private boolean occup; //true = occupée, false = vide

    
/*______________*/
//Constructeurs   
    /**
     * Constructeur par défaut sans paramètre
     */
    public CaseGrille(){
        super();
        occup = false;
    }
    
    /**
     * Constructeur avec coordonnées en paramètres
     * Initialise une case et initialise le booléen occup par défaut
     * @param x
     * @param y 
     */
    public CaseGrille(int x, int y){
        super(x, y);
        occup = false;
    }
    
    
    

/*______________*/
//Méthodes
    
    //Getters
    /**
     * Get valeur du booléen occup
     * @return 
     */
    public boolean getOccup(){
        return this.occup;
    }
    
    //Setters
    /**
     * Change valeur du booléen occup
     * @param occup 
     */
    public void setOccup(boolean occup){
        this.occup = occup;
    }
}
