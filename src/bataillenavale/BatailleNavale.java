/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bataillenavale;
import java.util.Scanner;

/**
 * Lancer la partie
 * @author Olivier
 */
public class BatailleNavale {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        String nom;
        
        System.out.println("Bataille Navale");
        System.out.println("Entrez le nom de la partie:");
        Scanner sc = new Scanner (System.in);
        nom = sc.nextLine();
        Partie partie = new Partie(nom);
        partie.lancerPartie();
        
    }
    
}
